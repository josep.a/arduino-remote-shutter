#include "Arduino.h"
#include <LiquidCrystal.h>
#include "TimeSelector.h"

// ------------------------ PUBLIC METHODS ------------------------
TimeSelector::TimeSelector(){
  reset();
}

void TimeSelector::reset(){
  hours = 0;
  minutes = 0;
  seconds = 0;
  milliseconds = 0;
  cursorPosition_ = 0;
}

void TimeSelector::increment(){
  changeSelector(true);
}

void TimeSelector::decrement(){
  changeSelector(false); 
}

void TimeSelector::moveCursorLeft(){
  if(cursorPosition_ > 0){
    Serial.print("Move cursor from ");
    Serial.print(cursorPosition_);
    Serial.print(" to left");
    cursorPosition_ -= 1;
  }
}
void TimeSelector::moveCursorRight(){
  if(cursorPosition_ < 9){
    Serial.print("Move cursor from ");
    Serial.print(cursorPosition_);
    Serial.print(" to right");
    cursorPosition_ += 1;
  }
}

void TimeSelector::printDelay(LiquidCrystal lcd){
  char timeStr[16] = "";
  //lcd.noCursor();
  lcd.setCursor(0, 0);
  lcd.print("shots delay:");
  lcd.setCursor(0, 1);

  snprintf(timeStr, 16, "%02dh%02dm%02d.%03ds >", hours, minutes, seconds, milliseconds);
  lcd.print(timeStr);

  int blinkPosition = cursorPosition_;
  if (cursorPosition_ == 9)
    blinkPosition = 14;
  else if (cursorPosition_ >= 6)
    blinkPosition += 3;
  else if (cursorPosition_  >= 4)
    blinkPosition += 2;
  else if (cursorPosition_  >= 2)
    blinkPosition += 1;
  
  lcd.setCursor(blinkPosition, 1);
  lcd.cursor();
}

int TimeSelector::toMillis(){
  return milliseconds +
         seconds * 1000 +
         minutes * 60 * 1000 + 
         hours * 60 * 60 * 1000;
}

int TimeSelector::cursorPosition(){
  return cursorPosition_;
}

// ------------------------ PRIVATE METHODS ------------------------
void TimeSelector::changeSelector(bool positive) {
  Serial.println("UP");
  int current_value;
  switch (cursorPosition_) {
    case 0:
      current_value = hours / 10;
      break;
    case 1:
      current_value = hours % 10;
      break;
    case 2:
      current_value = minutes / 10;
      break;
    case 3:
      current_value = minutes % 10;
      break;
    case 4:
      current_value = seconds / 10;
      break;
    case 5:
      current_value = seconds % 10;
      break;
    case 6:
      current_value = milliseconds / 100;
      break;
    case 7:
      current_value = milliseconds % 100;
      current_value /= 10;
      break;
    case 8:
      current_value = milliseconds % 10;
      break;
  }
  int sense = positive ? 1 : -1;
  setCurrentPosition(current_value + 1 * sense);
}

void TimeSelector::setCurrentPosition(const int value) {
  if(value < 0) return;
  
  switch (cursorPosition_) {
    case 0: // _XhXXmXX.XXXs
      if (hours % 10 + value * 10 <= 24) {
        hours %= 10;
        hours += value * 10;
      } else {
        hours %= 10;
      }
      break;
    case 1: // X_hXXmXX.XXXs
      hours = (hours / 10) * 10;
      if (hours / 10 + value <= 24) {
        hours += value;
      }
      break;
    case 2: // XXh_XmXX.XXXs
      minutes %= 10;
      if (value < 6)
        minutes += value * 10;
      break;
    case 3: // XXhX_mXX.XXXs
      minutes = (minutes / 10) * 10;
      if (value < 10)
        minutes += value;
      break;
    case 4: // XXhXXm_X.XXXs
      seconds %= 10;
      if (value < 6)
        seconds += value * 10;
      break;
    case 5: // XXhXXmX_.XXXs
      seconds = (seconds / 10) * 10;
      if ( value < 10 )
        seconds += value;
      break;
    case 6: // XXhXXmXX._XXs
      milliseconds %= 100;
      if (value < 10)
        milliseconds += value * 100;
      break;
    case 7: // XXhXXmXX.X_Xs
      milliseconds = (milliseconds / 100) * 100 + milliseconds % 10;
      if (value < 10)
        milliseconds += value * 10;
      break;
    case 8: // XXhXXmXX.XX_s
      milliseconds = (milliseconds / 10) * 10;
      if (value < 10)
        milliseconds += value;
      break;
  }
}

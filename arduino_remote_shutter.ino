#include <LiquidCrystal.h>
#include <IRremote.h>

const int receiver_pin = 3;
const int camera_pin = 2;
const int camera_press_time = 100; // ms to keep pressing the shutter ~30

struct DELAY_SELECTOR {
  int hours;
  int minutes;
  int seconds;
  int milliseconds;
};

int selecting_current_cursor_position = 0;

LiquidCrystal lcd(7, 8, 9, 10, 11, 12);
IRrecv irrecv(receiver_pin);
decode_results results;

int start_run = 0;

void up(const int cursor_position, struct DELAY_SELECTOR * delay_selector) {
  int current_value;
  switch (cursor_position) {
    case 0:
      current_value = delay_selector->hours / 10;
      set(cursor_position, delay_selector, current_value + 1);
      break;
    case 1:
      current_value = delay_selector->hours % 10;
      set(cursor_position, delay_selector, current_value + 1);
      break;
    case 2:
      current_value = delay_selector->minutes / 10;
      set(cursor_position, delay_selector, current_value + 1);
      break;
    case 3:
      current_value = delay_selector->minutes % 10;
      set(cursor_position, delay_selector, current_value + 1);
      break;
    case 4:
      current_value = delay_selector->seconds / 10;
      set(cursor_position, delay_selector, current_value + 1);
      break;
    case 5:
      current_value = delay_selector->seconds % 10;
      set(cursor_position, delay_selector, current_value + 1);
      break;
    case 6:
      current_value = delay_selector->milliseconds / 100;
      set(cursor_position, delay_selector, current_value + 1);
      break;
    case 7:
      current_value = delay_selector->milliseconds % 100;
      current_value /= 10;
      set(cursor_position, delay_selector, current_value + 1);
      break;
    case 8:
      current_value = delay_selector->milliseconds % 10;
      set(cursor_position, delay_selector, current_value + 1);
      break;
  }
}

void down(const int cursor_position, struct DELAY_SELECTOR * delay_selector) {
  int current_value;
  switch (cursor_position) {
    case 0:
      current_value = delay_selector->hours / 10;
      set(cursor_position, delay_selector, current_value - 1);
      break;
    case 1:
      current_value = delay_selector->hours % 10;
      set(cursor_position, delay_selector, current_value - 1);
      break;
    case 2:
      current_value = delay_selector->minutes / 10;
      set(cursor_position, delay_selector, current_value - 1);
      break;
    case 3:
      current_value = delay_selector->minutes % 10;
      set(cursor_position, delay_selector, current_value - 1);
      break;
    case 4:
      current_value = delay_selector->seconds / 10;
      set(cursor_position, delay_selector, current_value - 1);
      break;
    case 5:
      current_value = delay_selector->seconds % 10;
      set(cursor_position, delay_selector, current_value - 1);
      break;
    case 6:
      current_value = delay_selector->milliseconds / 100;
      set(cursor_position, delay_selector, current_value - 1);
      break;
    case 7:
      current_value = (delay_selector->milliseconds / 100) % 10;
      set(cursor_position, delay_selector, current_value - 1);
      break;
    case 8:
      current_value = delay_selector->milliseconds % 10;
      set(cursor_position, delay_selector, current_value - 1);
      break;
  }
}

void set(const int cursor_position, struct DELAY_SELECTOR * delay_selector, const int value) {
  if(value < 0) return;
  switch (cursor_position) {
    case 0: // _XhXXmXX.XXXs
      if (delay_selector->hours % 10 + value * 10 <= 24) {
        delay_selector->hours %= 10;
        delay_selector->hours += value * 10;
      } else {
        delay_selector->hours %= 10;
      }
      break;
    case 1: // X_hXXmXX.XXXs
      delay_selector->hours = (delay_selector->hours / 10) * 10;
      if (delay_selector->hours / 10 + value <= 24) {
        delay_selector->hours += value;
      }
      break;
    case 2: // XXh_XmXX.XXXs
      delay_selector->minutes %= 10;
      if (value < 6)
        delay_selector->minutes += value * 10;
      break;
    case 3: // XXhX_mXX.XXXs
      delay_selector->minutes = (delay_selector->minutes / 10) * 10;
      if (value < 10)
        delay_selector->minutes += value;
      break;
    case 4: // XXhXXm_X.XXXs
      delay_selector->seconds %= 10;
      if (value < 6)
        delay_selector->seconds += value * 10;
      break;
    case 5: // XXhXXmX_.XXXs
      delay_selector->seconds = (delay_selector->seconds / 10) * 10;
      if ( value < 10 )
        delay_selector->seconds += value;
      break;
    case 6: // XXhXXmXX._XXs
      delay_selector->milliseconds %= 100;
      if (value < 10)
        delay_selector->milliseconds += value * 100;
      break;
    case 7: // XXhXXmXX.X_Xs
      delay_selector->milliseconds = (delay_selector->milliseconds / 100) * 100 +
                                      delay_selector->milliseconds % 10;
      if (value < 10)
        delay_selector->milliseconds += value * 10;
      break;
    case 8: // XXhXXmXX.XX_s
      delay_selector->milliseconds = (delay_selector->milliseconds / 10) * 10;
      if (value < 10)
        delay_selector->milliseconds += value;
      break;
  }
}

void move_cursor_left(int * cursor_position) {
  Serial.println("Moving cursor to the left");
  if (*cursor_position > 0)
    *cursor_position -= 1;
}
void move_cursor_right(int * cursor_position) {
  Serial.println("Moving cursor to the right");
  if (*cursor_position < 9)
    *cursor_position += 1;
}

void translateIR(int * cursor_position, struct DELAY_SELECTOR * delay_selector) {
  switch (results.value) {
    case 0xFFA25D: Serial.println("POWER"); break;
    case 0xFFE21D: Serial.println("FUNC/STOP"); break;
    case 0xFF629D: Serial.println("VOL+"); break;
    case 0xFF22DD: move_cursor_left(cursor_position);    break;
    case 0xFF02FD: start_run = 1;    break;
    case 0xFFC23D: move_cursor_right(cursor_position);   break;
    case 0xFFE01F: down(*cursor_position, delay_selector);    break;
    case 0xFFA857: Serial.println("VOL-");    break;
    case 0xFF906F: up(*cursor_position, delay_selector);    break;
    case 0xFF9867: Serial.println("EQ");    break;
    case 0xFFB04F: Serial.println("ST/REPT");    break;
    case 0xFF6897: set(*cursor_position, delay_selector, 0);    break;
    case 0xFF30CF: set(*cursor_position, delay_selector, 1);    break;
    case 0xFF18E7: set(*cursor_position, delay_selector, 2);    break;
    case 0xFF7A85: set(*cursor_position, delay_selector, 3);    break;
    case 0xFF10EF: set(*cursor_position, delay_selector, 4);    break;
    case 0xFF38C7: set(*cursor_position, delay_selector, 5);    break;
    case 0xFF5AA5: set(*cursor_position, delay_selector, 6);    break;
    case 0xFF42BD: set(*cursor_position, delay_selector, 7);    break;
    case 0xFF4AB5: set(*cursor_position, delay_selector, 8);    break;
    case 0xFF52AD: set(*cursor_position, delay_selector, 9);    break;
    case 0xFFFFFFFF: Serial.println(" REPEAT"); break;
    default:
      Serial.println(results.value, HEX);
  }
}

void print_delay(const int cursor_position, const struct DELAY_SELECTOR delay_selector) {
  char time_str[14] = "";
  lcd.noCursor();
  lcd.setCursor(0, 0);
  lcd.print("shots delay:");
  lcd.setCursor(0, 1);

  snprintf(time_str, 14, "%02dh%02dm%02d.%03ds", delay_selector.hours, delay_selector.minutes, delay_selector.seconds, delay_selector.milliseconds);
  lcd.print(time_str);

  int blink_position = cursor_position;

  if (cursor_position >= 6)
    blink_position += 3;
  else if (cursor_position >= 4)
    blink_position += 2;
  else if (cursor_position >= 2)
    blink_position += 1;
  lcd.setCursor(blink_position, 1);
  lcd.cursor();
}

void select_delay(struct DELAY_SELECTOR * delay_selector) {
  int cursor_position = 0;

  print_delay(cursor_position, *delay_selector);

  do{
    if (irrecv.decode(&results)) {
      translateIR(&cursor_position, delay_selector);
      print_delay(cursor_position, *delay_selector);
      delay(200);
      irrecv.resume();
    }
  }while (!start_run);
}

void timelapse(int delay_between_shots) {
  int counter = 0;
  while (1) {
    digitalWrite(camera_pin, HIGH);
    delay(camera_press_time);
    digitalWrite(camera_pin, LOW);
    counter++;

    lcd.setCursor(0, 0);
    lcd.print("Shots taken: ");
    lcd.print(counter);
    delay(delay_between_shots);
  }
}

int parse_delay_selector(const struct DELAY_SELECTOR delay_selector){
  return delay_selector.milliseconds +
         delay_selector.seconds * 1000 +
         delay_selector.minutes * 60 * 1000 + 
         delay_selector.hours * 60 * 60 * 1000;
}

void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);
  lcd.clear();
  lcd.blink();
  irrecv.enableIRIn();
  pinMode(camera_pin, OUTPUT);
}

void loop() {
  struct DELAY_SELECTOR delay_selector = {0, 0, 0, 0};
  int delay_selected = 0;
  do{
    select_delay(&delay_selector);
    delay_selected = parse_delay_selector(delay_selector);
  }while(delay_selected==0);
  
  lcd.clear();
  lcd.noBlink();
  timelapse(delay_selected);
}

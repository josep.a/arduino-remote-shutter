#include <LiquidCrystal.h>
#include "TimeSelector.h"

#define RE_CLK 6
#define RE_DT 7
#define RE_SWITCH 8
#define SHUTTER 3

#define LCD_RS A1
#define LCD_E 4
#define LCD_D4 A2
#define LCD_D5 A3
#define LCD_D6 A4
#define LCD_D7 A5

// Constants for the rotary encoder
#define CW false
#define CCW true

enum STATES {
  SELECTING_TIME,
  SELECTING_SHOTS,
  TIMELAPSE,
  FINISHED,
  RESET
};

typedef struct Timelapse {
  int total_shots;
  int taken_shots;
  int delay_between_shots;
  unsigned long last_shot_time;
} Timelapse;

const int camera_press_time = 300; // ms to keep pressing the shutter

LiquidCrystal lcd(LCD_RS, LCD_E, LCD_D4, LCD_D5, LCD_D6, LCD_D7);
int state;
TimeSelector timeSelector;
Timelapse timelapse;

bool turnDetected;
bool rotationDirection;
bool pressed;
int selecting;

//states for debouncing
static uint16_t db_state_re = 0;
static uint16_t db_state_sw = 0;


void changeState(int newState){
  Serial.print("Changing state from ");
  Serial.print(state);
  Serial.print(" to ");
  Serial.println(newState);
  state = newState;
  
  lcd.clear();
  switch(newState){
    case SELECTING_TIME:
      timeSelector.reset();
      selecting = 0;
      timeSelector.printDelay(lcd);
      break;
    case SELECTING_SHOTS:
      timelapse.total_shots = 0;
      printNumberOfShots();
      break;
    case TIMELAPSE:
      timelapse.last_shot_time = 0;
      timelapse.taken_shots = 0;
      printDoneShots();
      break;
    case FINISHED:
      printFinished();
      break;
    case RESET:
      changeState(SELECTING_TIME);
      break;
  }
}


void parseInputs(){
  // based on https://www.best-microcontroller-projects.com/rotary-encoder.html
  db_state_re = (db_state_re<<1) | digitalRead(RE_CLK) | 0xe000;
  if(db_state_re == 0xf000){
    db_state_re = 0x000;
    rotationDirection = digitalRead(RE_DT);
    turnDetected = true;
    if(rotationDirection == CW)
      Serial.println("CW turn detected");
    else
      Serial.println("CCW turn detected");
  }

  db_state_sw = (db_state_sw<<1) | digitalRead(RE_SWITCH) | 0xe000;
  if(db_state_sw == 0xf000){
    db_state_sw = 0x000;
    pressed = true;
    Serial.println("Press detected");
  }
}


void setup() {
  Serial.begin(9600);
  
  lcd.begin(16, 2);

  pinMode(SHUTTER, OUTPUT);
  pinMode(RE_CLK, INPUT);
  pinMode(RE_DT, INPUT);
  pinMode(RE_SWITCH, INPUT_PULLUP);

  changeState(RESET);
}


void selectTimeIteration(){
  if(timeSelector.cursorPosition() == 9){
    if(pressed){
      pressed = false;
      changeState(SELECTING_SHOTS);
      return;
    }
  } else if (pressed){
    pressed = false;
    selecting = !selecting;
    if(selecting){
      lcd.blink();
      Serial.println("Change to selecting");
    }else{
      lcd.noBlink();
      Serial.println("Change to move");
    }
  }

  if(turnDetected){
    turnDetected = false;
    if (selecting){
      if (rotationDirection == CW){
        Serial.println("Increment");
        timeSelector.increment();
      } else {
        Serial.println("Decrement");
        timeSelector.decrement();
      }
    } else {
      if (rotationDirection == CW){
        Serial.println("Move right");
        timeSelector.moveCursorRight();
      } else {
        Serial.println("Move left");
        timeSelector.moveCursorLeft();
      }
    }
    timeSelector.printDelay(lcd);
  }
}

void printNumberOfShots(){
  char str[5] = "0000";
  snprintf(str, 5, "%04d", timelapse.total_shots);
  
  lcd.setCursor(0, 0);
  lcd.noCursor();
  lcd.clear();
  lcd.print("# of shots:");
  lcd.setCursor(0, 1);
  lcd.print(str);
}


void selectShotsIteration(){
  if(pressed) {
    pressed = false;
    timelapse.delay_between_shots = timeSelector.toMillis();
    changeState(TIMELAPSE);
    return;
  }

  if(turnDetected){
    turnDetected = false;
    if(rotationDirection == CW && timelapse.total_shots < 9999) {
      Serial.println(timelapse.total_shots);
      timelapse.total_shots += 1;
    } else if(rotationDirection == CCW && timelapse.total_shots > 0) {
      Serial.println(timelapse.total_shots);
      timelapse.total_shots -= 1;  
    }
    printNumberOfShots();
  }
}

void printDoneShots(){
  lcd.clear();
  lcd.noCursor();
  lcd.setCursor(0, 0);
  lcd.print("Shots taken: ");
  lcd.print(timelapse.taken_shots);
}

void timelapseIteration(){  
  if(timelapse.taken_shots >= timelapse.total_shots){
    changeState(FINISHED);
    return;
  }

  if(timelapse.last_shot_time + timelapse.delay_between_shots > millis()){
    return; // next loop
  }
  
  digitalWrite(SHUTTER, HIGH);
  delay(camera_press_time);
  digitalWrite(SHUTTER, LOW);
  timelapse.last_shot_time = millis();

  timelapse.taken_shots += 1;
  printDoneShots();
}

void printFinished(){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Finished ");
  lcd.print(timelapse.taken_shots);
  lcd.print(" shots");
  lcd.setCursor(0, 1);
  lcd.print("Press OK...");
}

void finished(){
  if(pressed){
    pressed = false;
    changeState(RESET);
  }
}

void loop() {
  parseInputs();

  if(state == SELECTING_TIME){
    selectTimeIteration();
  } else if(state == SELECTING_SHOTS){
    selectShotsIteration();
  } else if(state == TIMELAPSE){
    timelapseIteration();
  } else if (state == FINISHED) {
    finished();
  }
}

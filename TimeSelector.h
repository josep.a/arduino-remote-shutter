/*
  TimeSelector.h - Library for selecting a time delay
  Created by Josep Alòs Pascual, December 24, 2019.
  Released into the public domain.
*/

#ifndef __TIME_SELECTOR_H__
#define __TIME_SELECTOR_H__

#include "Arduino.h"
#include <LiquidCrystal.h>

class TimeSelector {
  public:
    TimeSelector();
    void increment();
    void decrement();
    void moveCursorLeft();
    void moveCursorRight();
    void printDelay(LiquidCrystal lcd);
    int toMillis();
    int cursorPosition();
    void reset();
  private:
    void setCurrentPosition(const int value);
    void changeSelector(bool positive);
    int hours;
    int minutes;
    int seconds;
    int milliseconds;
    int cursorPosition_;
};
#endif
